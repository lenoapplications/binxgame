﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ButtonHighlightListener : MonoBehaviour, IPointerEnterHandler,IPointerExitHandler
{
    public float buttonWidth { get { return 350; } }
    public float buttonHeight { get { return 120; } }
    public int fontSize { get { return 30; } }

    public void OnPointerEnter(PointerEventData eventData)
    {
        GetComponent<RectTransform>().sizeDelta = new Vector2(buttonWidth + 100,buttonHeight);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        GetComponent<RectTransform>().sizeDelta = new Vector2(buttonWidth, buttonHeight);
    }
}
