﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour
{
  
    [SerializeField]
    public Button playButton; 

    [SerializeField]
    public Button quitButton;

    

    void Start()
    {
        SetupMainMenu();
    }

    public void StartOnClick() 
    {
        SceneManager.LoadScene("Game");
    }

    public void QuitOnClick() 
    {
        Debug.Log("quit");
    }



    private void SetupMainMenu()
    {
        PositionButtons();    
    }

    private void PositionButtons() 
    {

        PositionPlayButton();
        PositionQuitButton();
    }

    private void PositionPlayButton() 
    {
        Vector3 playPosition = playButton.transform.position;
        playPosition.x = this.transform.position.x;
        playPosition.y = this.transform.position.y  + playButton.GetComponent<ButtonHighlightListener>().buttonHeight;

        changeHeightOfButton(playButton);
        ChangeButtonFontSize(playButton);
        playButton.transform.position = playPosition;
    }
    private void PositionQuitButton() 
    {
        Vector3 quitPosition = quitButton.transform.position;
        float spaceBetween = playButton.GetComponent<RectTransform>().sizeDelta.y;
        quitPosition.x = this.transform.position.x;
        quitPosition.y = this.transform.position.y + playButton.GetComponent<ButtonHighlightListener>().buttonHeight - (spaceBetween * 1.25f);

        changeHeightOfButton(quitButton);
        ChangeButtonFontSize(quitButton);
        quitButton.transform.position = quitPosition;
    }


    private void changeHeightOfButton(Button button)
    {
        button.GetComponent<RectTransform>().sizeDelta = new Vector2(playButton.GetComponent<ButtonHighlightListener>().buttonWidth, playButton.GetComponent<ButtonHighlightListener>().buttonHeight);
    }
    private void ChangeButtonFontSize(Button button) 
    {
        Text text = button.transform.Find("Text").GetComponent<Text>();
        text.fontSize = playButton.GetComponent<ButtonHighlightListener>().fontSize;
    }

  

  
}
