﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainController : MonoBehaviour
{
    private TerrainDecorator terrainDecorator;

    public float wallThickness 
    {
        get{return 0.5f;}
        set{}
    }
    public float wallHeight
    {
        get{return 5;}
        set{}
    }


    [SerializeField]
    public GameObject wallPrefab;

    [SerializeField]
    public GameObject yelloBoxPrefab;

    [SerializeField]
    public GameObject greenBoxPrefab;

    [SerializeField]
    public GameObject damagableBox;

    [SerializeField]
    public GameObject ammunition;

    [SerializeField]
    public float terrainWidth;

    [SerializeField]
    public float terrainDepth;



    void Start()
    {
        this.terrainDecorator = new TerrainDecorator(this);
        CreateTerrain();
    }

    // Update is called once per frame
    void Update()
    {
        terrainDecorator.GenerateAmmunition();

    }
    public GameObject GeneratePrefab(GameObject gameObject,Vector3 position,Quaternion quaternion)
    {
        return Instantiate(gameObject,position,quaternion);
    }

    private void CreateTerrain()
    {
        SetTerrainWidthAndHeight();
        SetBorderWalls();
        DecorateTerrain();
        
    
    }
    private void SetTerrainWidthAndHeight()
    {
         terrainDecorator.SetTerrainWidthAndHeight();
    }
    private void SetBorderWalls()
    {
        terrainDecorator.CreateBorderWalls( Instantiate(wallPrefab,new Vector3(0,0,0),Quaternion.identity),"horizontal", 1);
        terrainDecorator.CreateBorderWalls( Instantiate(wallPrefab,new Vector3(0,0,0),Quaternion.identity),"horizontal", -1);
        terrainDecorator.CreateBorderWalls( Instantiate(wallPrefab,new Vector3(0,0,0),Quaternion.identity),"vertical", -1);
        terrainDecorator.CreateBorderWalls( Instantiate(wallPrefab,new Vector3(0,0,0),Quaternion.identity),"vertical", 1);
    }
    private void DecorateTerrain()
    {
        terrainDecorator.GenerateTerrainWalls();
        terrainDecorator.GenerateGreenYellowAndPurpleCubes();
    }
}
