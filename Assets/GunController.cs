﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunController : MonoBehaviour
{

    [SerializeField]
    public int poolSize;

    [SerializeField]
    public GameObject bullet;

    public GameObject hole;


    private Queue<GameObject> poolDictionary;
    

    // Start is called before the first frame update
    void Start()
    {
        this.poolDictionary = new Queue<GameObject>();

        for(int i = 0; i < poolSize; ++i)
        {
            GameObject obj = Instantiate(bullet);
            obj.SetActive(false);
            this.poolDictionary.Enqueue(obj);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SpawnFromPool()
    {
        GameObject objectToSpawn = poolDictionary.Dequeue();
        objectToSpawn.SetActive(true);
        objectToSpawn.transform.position = new Vector3(hole.transform.position.x,hole.transform.position.y,hole.transform.position.z);
        Rigidbody objectRigidBody = objectToSpawn.GetComponent<Rigidbody>();
        objectRigidBody.AddForce(hole.transform.forward * 2000);

        poolDictionary.Enqueue(objectToSpawn);
    }
}
