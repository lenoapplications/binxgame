﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour
{

    private EnemyPath enemyPath;


    void Start()
    {
        this.enemyPath = new EnemyPath(this);
    }

    // Update is called once per frame
    void Update()
    {
        this.enemyPath.Move();
    }


 

}
