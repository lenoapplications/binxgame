﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamagableBoxController : MonoBehaviour
{
    // Start is called before the first frame update

    public float health = 100;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        CheckHealth();
    }

    void OnCollisionEnter(Collision collision)
    {
        Debug.Log($"{health}");
        health -= 10;
    }

    private void CheckHealth()
    {
        if (health < 1)
        {
            Object.Destroy(this.gameObject);
        }
    }
}
