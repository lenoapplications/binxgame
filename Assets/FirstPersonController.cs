﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstPersonController : MonoBehaviour
{
    // Start is called before the first frame update
    private readonly FirstPersonInputListener firstPersonInputListener;
    public GameObject eyes{get; private set;}

    [SerializeField]
    public float speed = 10;
    [SerializeField]
    public float mouseSensitivity = 5.0f;

    public GunController gunController;

    
    public FirstPersonController()
    {
        this.firstPersonInputListener = new FirstPersonInputListener(this);
    }


    void Start()
    {
        this.gunController = GameObject.Find("Gun").GetComponent<GunController>();
        SetupFirstPerson();
        HideMouse();

    }

    // Update is called once per frame
    void Update()
    {
        firstPersonInputListener.ProcessInputs();
    }

    private void SetupFirstPerson()
    {
        eyes = transform.Find("Eyes").gameObject;
        SetEyesPosition();
    }

    private void SetEyesPosition()
    {
        Vector3 eyesPosition = eyes.transform.localPosition;
        float additionalHeightOfSphere = 0.5f;
        eyesPosition.y = 0.5f + additionalHeightOfSphere;

        eyes.transform.localPosition = eyesPosition;
    }

    private void HideMouse()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }
}
