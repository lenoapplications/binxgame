﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyPath
{
    private readonly EnemyController enemyController;
    private readonly Rigidbody rigidbody;
    private Vector3 currentDirection;

    private NavMeshAgent navMeshAgent;
    private bool inCoRoutine;

    

   public EnemyPath(EnemyController controller)
   {
       this.enemyController = controller;
       this.currentDirection = Vector3.right;
       this.rigidbody = controller.GetComponent<Rigidbody>();
       this.navMeshAgent = controller.GetComponent<NavMeshAgent>();
   }


   public void Move()
   {
        if(!inCoRoutine)
        {
            enemyController.StartCoroutine(DoSomething());
        }
   }


    Vector3 GetNewRandomPosition()
    {
        float x = Random.Range(-20,20);
        float z = Random.Range(-20,20);

        Vector3 pos = new Vector3(x,1,z);
        return pos;
    }

    IEnumerator DoSomething()
    {
        inCoRoutine = true;
        yield return new WaitForSeconds(2);
        GetNewPath();
        inCoRoutine = false;
    }

    void GetNewPath()
    {
        navMeshAgent.SetDestination(GetNewRandomPosition());
    }




}
