﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstPersonInputListener
{
    private Vector2 mouseLook;

    private readonly FirstPersonController firstPersonController;


    public FirstPersonInputListener(FirstPersonController firstPersonController)
    {
        this.firstPersonController = firstPersonController;
        this.mouseLook = new Vector2(0,0);
    }

    public void ProcessInputs()
    {
        CheckKeys();
        CheckMouse();
        CheckMouseButtons();
    }

    private void CheckKeys()
    {
        float vertical = Input.GetAxis("Vertical") * firstPersonController.speed;
        float horizontal = Input.GetAxis("Horizontal") * firstPersonController.speed;

        vertical *= Time.deltaTime;
        horizontal *= Time.deltaTime;

        firstPersonController.transform.Translate(horizontal,0,vertical);
    }
    private void CheckMouse()
    {
        Vector2 mouseDelta = new Vector2(Input.GetAxis("Mouse X"),Input.GetAxis("Mouse Y"));
        mouseDelta *= firstPersonController.mouseSensitivity;
     

        firstPersonController.eyes.transform.Rotate(mouseDelta.y * Vector3.right);
        firstPersonController.transform.Rotate(mouseDelta.x * Vector3.up);
    }

    private void CheckMouseButtons()
    {
        if(Input.GetMouseButton(0))
        {
            this.firstPersonController.gunController.SpawnFromPool();
        }
    }
    
}
