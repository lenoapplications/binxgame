using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainPositionsHolder
{
    public Vector3 bottomMiddlePosition {get; private set;}
    public Vector3 upMiddlePosition {get; private set;}
    public Vector3 leftMiddlePosition {get; private set;}
    public Vector3 rightMiddlePosition {get; private set;}
    public Vector3 downLeftAngle {get; private set;}
    public Vector3 upRightAngle {get; private set;}


    public TerrainPositionsHolder(TerrainController terrainController)
    {
        SetMiddlePosition(terrainController);
    }
    private void SetMiddlePosition(TerrainController terrainController)
    {
        float terrainWidth = terrainController.terrainWidth;
        float terrainDepth = terrainController.terrainDepth;
        float terrainHeight = (terrainController.transform.localScale.y / 2f) + (terrainController.wallHeight / 2f);

        bottomMiddlePosition = new Vector3(0,terrainHeight,-(terrainDepth / 2));
        downLeftAngle = new Vector3(-terrainWidth / 2,terrainHeight,-terrainDepth / 2);
        upRightAngle = new Vector3(terrainWidth / 2,terrainHeight,terrainDepth / 2);
    }

}