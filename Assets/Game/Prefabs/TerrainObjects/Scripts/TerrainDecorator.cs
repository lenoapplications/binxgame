﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainDecorator
{

    private readonly TerrainController terrainController;
    private readonly TerrainPositionsHolder terrainPositionsHolder;

    public TerrainDecorator(TerrainController terrainController)
    {
        this.terrainController = terrainController;
        this.terrainPositionsHolder = new TerrainPositionsHolder(terrainController);
    }
  
    public void SetTerrainWidthAndHeight()
    {
        terrainController.transform.localScale = new Vector3(terrainController.terrainWidth,
                                                             terrainController.transform.localScale.y,
                                                             terrainController.terrainDepth);
    }


    public void CreateBorderWalls(GameObject wall, string type,float side)
    {
        Vector3 wallPosition = new Vector3();
        Vector3 wallScale;
        BoxCollider boxCollider = wall.GetComponent<BoxCollider>();

        if (type == "horizontal")
        { 
            wallPosition.z = ( -(terrainController.terrainDepth / 2f) - (terrainController.wallThickness / 2f) ) * side;
            wallScale = new Vector3(terrainController.terrainWidth,terrainController.wallHeight,terrainController.wallThickness);
            boxCollider.size = new Vector3(terrainController.terrainWidth + 2, terrainController.wallHeight, terrainController.wallThickness + 2);
        }
        else
        {
            wallPosition.x = ( -(terrainController.terrainWidth / 2f) - (terrainController.wallThickness / 2f) ) * side;
            wallScale = new Vector3(terrainController.wallThickness,terrainController.wallHeight,terrainController.terrainWidth);
            boxCollider.size = new Vector3(terrainController.wallThickness + 2, terrainController.wallHeight, terrainController.terrainWidth + 2);
        }
        wallPosition.y =(terrainController.transform.localScale.y / 2f) + (terrainController.wallHeight / 2f);
        wall.transform.localPosition = wallPosition;
        wall.transform.localScale = wallScale;
    }


    public void GenerateTerrainWalls()
    {
        float xPositionRange = terrainController.transform.localScale.x / 2;
        float zPositionRange = terrainController.transform.localScale.z / 2;
        int count = (int)((terrainController.terrainDepth * terrainController.terrainWidth) * 0.5f);

        for (int i = 0; i < count; ++i){
            Vector3 position = new Vector3(0,0,0);
            Vector3 scale = new Vector3(0,0,0);

            GenerateScale(ref scale,0.5f,0.09f,terrainController.wallHeight);
            GeneratePosition(ref position,xPositionRange,zPositionRange,scale.x + scale.z);
            float diagonal =Mathf.Sqrt( (terrainController.wallHeight * terrainController.wallHeight) + (scale.x * scale.x) );  
            
            if(CheckIfPositionIsFree(position,diagonal / 2))
            {
                GameObject wall = terrainController.GeneratePrefab(terrainController.wallPrefab,position,Quaternion.identity);
                wall.transform.localScale = scale;
            }
        }  
    }

    public void GenerateGreenYellowAndPurpleCubes() 
    {
        float xPositionRange = terrainController.transform.localScale.x / 2;
        float zPositionRange = terrainController.transform.localScale.z / 2;
        int count = (int)( (terrainController.terrainDepth * terrainController.terrainWidth) * 0.08f);
        
        for (int i = 0; i < count; ++i)
        {
            Vector3 position = new Vector3(0, 0, 0);
            Vector3 scale = new Vector3(1, 1, 1);
            GeneratePosition(ref position ,xPositionRange,zPositionRange, 1);
            position.y = 1;

            if (CheckIfPositionIsFree(position,2.5f))
            {
                GameObject box = terrainController.GeneratePrefab(RandomPickBox(), position, Quaternion.identity);
                box.transform.localScale = scale;
            }
        }
    }

    public void GenerateAmmunition()
    {
        float xPositionRange = terrainController.transform.localScale.x / 2;
        float zPositionRange = terrainController.transform.localScale.z / 2;

        if(GameObject.FindGameObjectsWithTag("Ammunition").Length < 10 && (Time.time - AmmunitionController.ammunationTimeCreated) > 20) 
        {
            Vector3 position = new Vector3(0, 0, 0);
            Vector3 scale = new Vector3(1, 1, 1);
            GeneratePosition(ref position,xPositionRange,zPositionRange, 2);
            position.y = 1f;

            if (CheckIfPositionIsFree(position,2)) 
            {
                Debug.Log("Generated ammunition");
                terrainController.GeneratePrefab(terrainController.ammunition, position, Quaternion.identity);
                AmmunitionController.ammunationTimeCreated = Time.time;
            }
        }
    }

    private void GeneratePosition(ref Vector3 position,float xPositionRange,float zPositionRange,float additional)
    {
        float x = Random.Range(-xPositionRange + additional, xPositionRange - additional);
        float y= (terrainController.transform.localScale.y / 2f) + (terrainController.wallHeight / 2f);
        float z = Random.Range(-zPositionRange + additional, zPositionRange - additional); 

        position = new Vector3(x,y,z);
    }
    private void GenerateScale(ref Vector3 scale,float from,float to,float height)
    {
        float x = Random.Range(from, to * terrainController.terrainWidth);
        float z = Random.Range(from, to * terrainController.terrainDepth);
        scale = new Vector3(x,height,z);
    }

    private bool CheckIfPositionIsFree(Vector3 position ,float radius)
    {
       foreach(Collider coll in Physics.OverlapSphere(position,radius))
        {
            if(!coll.gameObject.name.Equals("Floor"))
            {
                return false;
            } 
        }
        return true;
    }
   
    private GameObject RandomPickBox() 
    {
        switch (Random.RandomRange(0, 3))
        {
            case 1: return terrainController.yelloBoxPrefab;
            case 2: return terrainController.damagableBox;
            default: return terrainController.greenBoxPrefab;
        }

    }
}
